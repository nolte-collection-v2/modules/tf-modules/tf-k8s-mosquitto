
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "mosquitto"
  repository = "https://halkeye.github.io/helm-charts/"
  chart      = "mosquitto"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
