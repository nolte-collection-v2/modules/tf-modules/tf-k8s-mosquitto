variable "namespace" {
  default = ""
}

variable "chart_version" {
  default = "0.2.0"
}

variable "extra_values" {
  default = {}
}
